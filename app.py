from flask  import Flask,render_template,request,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
from forms import RegistrationForm,LoginForm
from passlib.hash import pbkdf2_sha512
import os
from models import User
from flask_login import LoginManager,login_user,current_user,login_required,logout_user
app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://aman:aman@localhost/portalsystem4"
db = SQLAlchemy(app)
db.init_app(app)
with app.app_context():
    db.create_all()
login_instance=LoginManager(app)
login_instance.init_app(app)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


@login_instance.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/', methods=['GET','POST'])
def index():
    reg_form=RegistrationForm()
    if request.method=='POST' and reg_form.validate_on_submit():
        username=reg_form.username.data
        password=reg_form.password.data
        hashed_password=pbkdf2_sha512.hash(password)
        user=User(username=username,password=hashed_password)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('login'))

    return render_template('registration.html',form=reg_form)
        

@app.route('/login' , methods=['GET','POST'])
def login():
    login_form=LoginForm()

    if request.method=='POST' and login_form.validate_on_submit():

        user_object=User.query.filter_by(username=login_form.username.data).first()
        login_user(user_object)
        return current_user.username+ ' the password is '+current_user.password
        
        
    return render_template("login.html",form=login_form)





        



    

